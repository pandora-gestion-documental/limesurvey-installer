#!/bin/bash
# LimeSurvey CE Installer

version=$1
db_name=$2
db_username=$3

. /etc/os-release
os="$NAME $VERSION_ID"
case $os in
	"Ubuntu 18.04")
		chmod +x ./os/limesurvey-ce-ubuntu-18-04.sh
		sudo sh os/limesurvey-ce-ubuntu-18-04.sh $version $db_name $db_username;;
	"Ubuntu 20.04")
		chmod +x ./os/limesurvey-ce-ubuntu-20-04.sh
		sudo sh os/limesurvey-ce-ubuntu-20-04.sh $version $db_name $db_username;;
	*)
		printf "\033[1;31mError: $version is not currently supported for AtoM 2.6.\033[0m\n"
		exit
esac