#!/bin/bash
# LimeSurvey CE Ubuntu 20.04 Installer

version=$1
zip=${version##*/}
db_name=$2
db_username=$3

# Updating and upgrading software
apt update -y
apt upgrade -y

# Install unzip
apt install -y unzip

# Install MySQL
apt install -y mysql-server
sudo mysql_secure_installation

# Create database
echo "Choose the password for the limesurvey database user:"
read password
mysql -h localhost -u root -p -e "CREATE DATABASE $db_name"
mysql -h localhost -u root -p -e "CREATE USER '$db_username'@'localhost' IDENTIFIED BY '$password';"
mysql -h localhost -u root -p -e "GRANT ALL PRIVILEGES ON $db_name.* TO '$db_username'@'localhost';"
mysql -h localhost -u root -p -e "FLUSH PRIVILEGES;"

# Install PHP 7.4 and required modules
apt install -y php php-common php-cli php-mbstring php-xml php-mysql php-gd php-zip php-ldap php-imap

# Start and enable Apache and MySQL
systemctl start apache2
systemctl enable apache2
systemctl start mysql
systemctl enable mysql

# Download LimeSurvey CE
wget $version
unzip $zip
cp -r limesurvey /var/www/html/

# Update filesystem permissions
chown www-data:www-data -R /var/www/html/limesurvey/

# Setting up Apache
touch /etc/apache2/sites-available/limesurvey.conf
echo "<VirtualHost *:80>
ServerAdmin gcastellano@nosturi.es
DocumentRoot /var/www/html/limesurvey/
<Directory /var/www/html/limesurvey/>
Options FollowSymLinks
AllowOverride All
</Directory>
ErrorLog /var/log/apache2/lime-error_log
CustomLog /var/log/apache2/lime-access_log common
</VirtualHost>" >> /etc/apache2/sites-available/limesurvey.conf
a2ensite limesurvey
systemctl reload apache2
systemctl restart apache2

# Promtp the user to run the web installer
printf "\033[1;32mWe are almost done!\nOpen your browser and connect to the <your-host>/limesurvey/admin in order to complete the installation.\033[0m\n"
