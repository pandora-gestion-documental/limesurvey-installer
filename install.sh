#!/bin/bash
# LimeSurvey CE Installer for GNU/Linux

usage="LimeSurvey CE installer for GNU/Linux

Usage: $(basename "$0") [-h]  [-v release_name] [-db db_name] [-u db_username]

Where:
	-h	Display this help text.
	-v	Set the version to install (lts, unstable or dev). Default (recommended): lts.
	-db Set the database name. Default: limesurvey_db.
	-u	Set the database username. Default: limesurvey_user."

version=lts
db_name=limesurvey_db
db_username=limesurvey_user

while getopts ':hv:' option; do
	case "$option" in
		h)
			echo "$usage"
			exit;;
		v)
			version=$OPTARG;;

		db)
			db_name=$OPTARG;;

		u)
			db_username=$OPTARG;;

		:)
			printf "\033[1;31mmissing argument for -%s\033[0m\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1;;
		\?)
			printf "\033[1;31millegal option: -%s\033[0m\n" "$OPTARG" >&2
			echo "$usage" >&2
			exit 1;;
	esac
done

shift $((OPTIND - 1))

case $version in
	lts)
		printf "Installing LimeSurvey CE LTS...\n"
		chmod +x ./os/system.sh
		/bin/bash ./os/system.sh 'https://download.limesurvey.org/lts-releases/limesurvey3.27.2+210608.zip' $db_name $db_user;;
	unstable)
		printf "Installing LimeSurvey CE Unstable Release...\n"
		chmod +x ./os/system.sh
		/bin/bash ./os/system.sh 'https://download.limesurvey.org/latest-stable-release/limesurvey5.0.3+210609.zip' $db_name $db_user;;
	dev)
		printf "LimeSurvey CE Development Release...\n"
		chmod +x ./os/system.sh
		/bin/bash ./os/system.sh 'https://download.limesurvey.org/latest-stable-release/limesurvey5.0.3+210609.zip' $db_name $db_user;;
	*)
		echo "\033[1;31mPlease enter a valid version (LTS, Unstable or Dev).\033[0m\n";;
esac
